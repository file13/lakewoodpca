# lakewoodpca

This is the repo for our church website, [Lakewood PCA](http://lakewoodpca.com/.)

## Source

This is just static HTML generated with [selmer](https://github.com/yogthos/Selmer) in Clojure.

