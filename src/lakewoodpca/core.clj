(ns lakewoodpca.core
  (:require [selmer.parser :refer :all]
            )
  (:gen-class))

(selmer.parser/set-resource-path! "D:/code/clojure/lakewoodpca/resources")
(selmer.parser/cache-off!)

(defn gen
  "Generate a filename from a template."
  [filename]
  (spit (str "resources/" filename)
        (render-file (str "templates/" filename) {})))

(defn -main
  "Generate the entire website."
  [& args]
  (let [pages ["index.html"
               "ourchurch.html"
               "denomination.html"
               "contact.html"
               "qanda.html"
               "school.html"
               "404.html"
               ]]
    (map gen pages)))

(-main)
